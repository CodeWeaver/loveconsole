Description
========

`loveconsole` is a library that defines a `Console` class for [LÖVE](https://love2d.org/) (11.4).

A console object supports text *input / output*, where inputs are forwarded to a callback function *you* provide (so you can do anything you want with them!), and outputs can be specially-formatted (such as with color, highlighting, aligning, etc).

Consoles have a *Canvas* component so they can be drawn with a single call inside `love.draw()`, regardless of whether the console is "open" or not. You could even use a "closed" console to display texts such as leaderboards, or speech boxes!

The console *Canvas* is drawn in a performance-friendly manner that takes care of things such as text wrapping.

Documentation
==========

### **Console**.newConsole(*args*)

Returns new Console object.

`args` *(table: { field="value", ... })*
- Initializes any fields you pass.
- **default**: `nil` (This console is initialized with all default values)

Console objects have the following public fields that can be redefined through `args`:
- `backgroundTexture` *(Texture)*
	- **default**: window-sized blank Canvas
- `font` *(Font)*
	- **default**: default LÖVE font
- `fontColor` *(table: { red, green, blue, alpha })*
	- **default**: { 1, 1, 1, 1 } (white)
- `historyLimit` *(number)*
	- Maximum number of lines recorded in console.
	- Oldest entries are removed when number of lines reaches `historyLimit`
	- **default**: `false` (no limit)

*Lua syntactic sugar:* You can pass a table as the first and only argument by calling a function with braces instead of parentheses. This is useful for passing named arguments to this class, like so:
```lua
Console.newConsole{ fontColor = {0.5, 0.5, 0.5, 1}, historyLimit = 30 }
```

### **Console**:write(*line*)

Writes a line of text to the console.
`line` can be a *string* or a *table*.

If `line` is a string, the line will be output to the console using the console's default formatting.

If `line` is a key/value table, it will be interpreted as an *FText* object.
If `line` is a sequence table, it will be interpreted as a sequence of *strings* and *FText* objects.
`FText` ("Formatted Text") is an internal class with the following fields:
- `string` *(string)*
	- **default**: ""
- `font` *(Font)*
	- **default**: console.font
- `color` *(table: { red, green, blue, alpha })*
	- **default**: console.fontColor
- `highlight` *(table: { red, green, blue, alpha })*
	- **default**: { 0, 0, 0, 0 } (none)
- `align` *(AlignMode)*
	- 'left', 'center', or 'right'
	- **default**: 'left'
- `em` *(string)*
	- Flagstring which defines the emphasis for this text.
	- If `em` contains:
		- 'u' = underline
		- 'o' = omit (strikethrough)
	- **default**: "" (none)

Example:
```lua
c = Console.newConsole()
c:write{ "This is default text ",
	{ string="while this is formatted text", color={ 0.5, 0.5, 0.5 }, em='u' },
	"; more normal text."
}
```

### **Console**:open(*killKey, read*)

Cedes control of the keyboard and mouse to the console.

`killKey` *(KeyConstant)*
- Closes the console when pressed.
- **default**: 'escape'
	- Pass `nil` to use default
	- Pass `false` to disable killKey

`read` *(function(line))*
- This is a callback function *you* provide to handle each line of input by the player.
- It must accept one argument (the line of text entered).

**Note:** This method does not draw the console on-screen. The console can be drawn whether it's open or closed by calling `Console:getDrawable()` (defined below) inside `love.draw()`. You retain full control over when and how the console gets drawn.

### **Console**:close()

Restores control of the keyboard and mouse to the `love` callbacks.

### **Console**:getDrawable()

Returns a *Drawable* console object for displaying via `love.draw()`.

For performance, `getDrawable()` redraws a *Drawable* only if the console's state has changed since the last call to `getDrawable()`. Therefore it should be safe to draw a console with no further care taken by the programmer. If this class is yet ill-performant then please submit an Issue or suggested fix to the GitLab repo!

### **Console**.isOpen

Convenient property to check whether a console is open. This field should not be manually set.

Future support
==========
- Adding bold ('b'), italics ('i'), subscript ('s'), and superscript ('S') handling for `FText.em`
- Support for '\\n' and '\\t' characters
- Maintaining this library for future versions of LÖVE

I'm new to git and open-source projects so please be gentle
