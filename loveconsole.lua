--[[
LÖVE Console module
Logan "CodeWeaver" Hall
2017-9-19
]]

local Console = {}

--[[
Constructor
--]]
function Console.newConsole(args)
	--[[
	Creates new Console object.

	`args` (table: {field="value", ...}) will initialize any fields you pass. See all of Console's fields defined below this constructor.
		You ought to define backgroundTexture and font, if nothing else.
	--]]

	local self = {}
	setmetatable(self, { __index = Console })
	if args then
		for field,value in pairs(args) do self[field] = value end
	end
	return self
end


--[[
Fields
--]]
Console.backgroundTexture	= 	love.graphics.newCanvas()	-- Backdrop for Drawable (default is window-sized black rectangle)
Console.font			=	love.graphics.getFont()		-- Default font for console text
Console.fontColor		=	{1,1,1,1}			-- Default font color for console text
Console.tabLength		=	40				-- Tab characters are, at most, # pixels wide
Console.historyLimit		=	false				-- Maximum input/output history length (default is none)


--[[
Methods
--]]
function Console:write(line)
	--[[
	Writes a line (string or table) of text to the Console.

	If `line` is a string, the text will be written using the Console's default font and formatting.
	If `line` is a table, it should contain a sequence of FText objects.
		Any strings in this sequence will be written using the Console's default font and formatting.
		Any FText objects in this sequence use fields to define a string and the formatting with which to write it.
		FText fields:
			FText.string		(string) to be written
			FText.font			(Font)
			FText.align			(AlignMode) 'center', 'right', or default ('left')
			FText.em			(string) If line.em contains:
									'b', bold;
									'i', italics;
									'u', underline;
									's', subscript;
									'S', superscript;
									'o', omit (strikethrough)
			FText.color			(table: {red,green,blue,alpha})
			FText.highlight		(table: {red,green,blue,alpha})

	This function stores the line in an output buffer. The output is formatted and printed in Console:getDrawable().
	--]]

	self._redraw = true

	-- Ensure `line` is a sequence
	if type(line) ~= 'table' then	-- Not table
		line = { tostring(line) }
	elseif line[1] == nil then		-- Not sequence (object)
		line = { line }
	end

	-- Sanitize the line, ensuring all elements are FText objects
	for i,text in ipairs(line) do
		if type(text) ~= 'table' then
			line[i] = { string = tostring(text) }
		end
		for property,value in pairs(Console.FText) do
			if not line[i][property] then line[i][property] = value end
		end
	end

	table.insert(self._outputs, 1, line)
	if self.historyLimit and #self._outputs > self.historyLimit then table.remove(self._outputs) end
end

function Console:open(killKey, read)
	--[[
	Cedes control of the keyboard and mouse to the Console.

	`killKey` (KeyConstant) is a key that, when received, instantly closes the Console;
		default is 'escape'.
	`read(line)` (function) receives each line read from the player.
	--]]

	assert(read, "read() callback must be defined in Console:open()")
	if killKey == nil then killKey = 'escape' end
	love.keyboard.setTextInput(true)
	self.isOpen = true
	self._redraw = true

	function love.keypressed(key, scancode, isRepeat)
		if key == 'return' or key == 'kpenter' then
			table.insert(self._inputs, 1, self._text)
			if self._text == "_LCWH" then
				self:write("loveconsole by Logan \"CodeWeaver\" Hall")
			else
				read(self._text)
			end
			self._text = ""
			self._cursor = 1
			self._inputIndex = 0
		elseif key == 'backspace' then
			if self._cursor > 1 then
				if love.keyboard.isDown('lctrl') or love.keyboard.isDown('rctrl') then
					while not string.gmatch(self._text:sub(self._cursor, self._cursor), "%s") do
						self._text = self._text:sub(1, self._cursor-2) .. self._text:sub(self._cursor, -1)
						self._cursor = self._cursor - 1
					end
				else
					self._text = self._text:sub(1, self._cursor-2) .. self._text:sub(self._cursor, -1)
					self._cursor = self._cursor - 1
				end
			end
		elseif key == 'delete' then
			if self._cursor > 0 then
				if love.keyboard.isDown('lctrl') or love.keyboard.isDown('rctrl') then
					while not string.gmatch(self._text:sub(self._cursor, self._cursor), "%s") do
						self._text = self._text:sub(1, self._cursor-1) .. self._text:sub(self._cursor+1, -1)
					end 
				else
					self._text = self._text:sub(1, self._cursor-1) .. self._text:sub(self._cursor+1, -1)
				end
			end
		elseif key == 'up' then
			self._inputIndex = self._inputIndex + 1
			self._text = self._inputs[self._inputIndex]
			self._cursor = #self._text + 1
		elseif key == 'down' then
			if love.keyboard.isDown('lctrl') or love.keyboard.isDown('rctrl') then
				self._inputIndex = 0
			else
				self._inputIndex = self._inputIndex - 1
				self._text = self._inputs[self._inputIndex]
				self._cursor = #self._text + 1
			end
		elseif key == 'left' then
			if self._cursor > 1 then
				if love.keyboard.isDown('lctrl') or love.keyboard.isDown('rctrl') then
					while not string.gmatch(self._text:sub(self._cursor, self._cursor), "%s")  and self._cursor > 1 do
						self._cursor = self._cursor - 1
					end
				else
					self._cursor = self._cursor - 1
				end
			end
		elseif key == 'right' then
			if self._cursor <= #self._text then
				if love.keyboard.isDown('lctrl') or love.keyboard.isDown('rctrl') then
					while not string.gmatch(self._text:sub(self._cursor, self._cursor), "%s")  and self._cursor <= #self._text do
						self._cursor = self._cursor + 1
					end
				else
					self._cursor = self._cursor + 1
				end
			end
		elseif key == 'home' then
			self._cursor = 1
		elseif key == 'end' then
			self._cursor = #self._text + 1
		elseif key == 'pageup' then
			self._outputIndex = #self._outputs
		elseif key == 'pagedown' then
			self._outputIndex = 1
		end

		self._redraw = true

		if key == killKey then self:close() end
	end

	function love.textinput(text)
		if text ~= killKey then
			self._text = self._text:sub(1, self._cursor-1) .. text .. self._text:sub(self._cursor, -1)
			self._cursor = self._cursor + 1

			self._redraw = true
		end
	end

	function love.wheelmoved(x, y)
		self._outputIndex = self._outputIndex + y
		if self._outputIndex <= 1 then self._outputIndex = 1
		elseif self._outputIndex > #self._outputs then self._outputIndex = #self._outputs
		end

		self._redraw = true
	end
end

function Console:close()
	love.keypressed		= self._lovekeypressed
	love.keyreleased		= self._lovekeyreleased
	love.textinput		= self._lovetextinput
	love.mousepressed		= self._lovemousepressed
	love.mousereleased	= self._lovemousereleased
	love.wheelmoved		= self._lovewheelmoved
	self.isOpen = false
	self._redraw = true
end


--[[
Properties
--]]
function Console:getDrawable()
	if self._redraw then
		local canvas_width, canvas_height = self.backgroundTexture:getDimensions()
		self._canvas = love.graphics.newCanvas(canvas_width, canvas_height)
		
		self._canvas:renderTo(function ()
			--[[
			From the perspective of the user and Console:write(), "lines" are written to the Console.
			In the context of this function, "entries" are written to the console, while one entry
			may span several lines (if they must wrap).
			--]]

			love.graphics.draw(self.backgroundTexture, 0,0)
			love.graphics.setLineStyle('smooth')
			love.graphics.setLineWidth(1)

			local font_height	= self.font:getHeight()	-- Font height (in pixels)
			local x		= 5						-- Pixel x coordinate
			local y		= (	self.isOpen			-- Pixel y coordinate
							and	canvas_height - (font_height * 2) - 5
							or	canvas_height - 5
						)
			
			-- From bottom to top [of this canvas], print lines of text that are properly spaced apart by self.font's height
			for index=self._outputIndex, #self._outputs do
				-- Concat FText strings (to get wrap for entire _outputs entry)
				local wrap_string = ""
				for i,text in ipairs(self._outputs[index]) do
					wrap_string = wrap_string .. text.string
				end

				-- Get line-wrapped text
				local line_width,lines = self.font:getWrap(wrap_string, canvas_width - 10)

				x = 5
				y = y - (font_height * #lines)	-- Set y based on number of lines
				
				-- Variables
				local col	= 0	-- Text column
				local line	= 1	-- Line #

				-- Write each line
				for i,text in ipairs(self._outputs[index]) do
					-- If line is empty `nil`, convert to ""
					if not lines[line] then lines[line] = "" end

					-- Check for unspecified fields (use default if so)
					text.font = text.font or self.font
					text.color = text.color or self.fontColor

					-- Track how many more characters will be written to this line
					col = col + #text.string
					
					-- If over line width, split current FText in two; wrap second part onto next line
					local next_line = false	-- Flag for next line handling at the end of this loop 
					if col > #lines[line] then
						next_line = true
						local next_text = {}
						for property,value in pairs(text) do
							next_text[property] = value
						end
						next_text.string = text.string:sub(-(col - #lines[line]), -1)
						table.insert(self._outputs[index], i+1, next_text)
						text.string = text.string:sub(1, -(col - #lines[line]) - 1)
					end

					-- Pixel dimensions of current text
					local font_width = (text.font:getWidth(text.string))	--[[ TODO: font_width will be affected by tab characters or alignment ]]
					local font_height = text.font:getHeight()
					
					-- Highlight
					love.graphics.setColor(text.highlight)
					love.graphics.rectangle('fill', x,y, font_width, font_height)
					-- String
					love.graphics.setColor(text.color)
					love.graphics.setFont(text.font)
					love.graphics.printf(text.string, x,y, font_width, text.align)
					-- Formatting
					if text.em:match('b') then

					end
					if text.em:match('i') then

					end
					if text.em:match('u') then
						love.graphics.line(x, y + text.font:getAscent() + 3, x + font_width, y + text.font:getAscent() + 3)
					end
					if text.em:match('s') then

					end
					if text.em:match('S') then

					end
					if text.em:match('o') then
						love.graphics.line(x, y + text.font:getAscent() / 2, x + font_width, y + text.font:getAscent() / 2)
					end

					x = x + font_width	-- Adjust x
					
					-- If 
					if next_line then
						col	= 0
						x	= 5
						y	= y + font_height
						line	= line + 1
					end
				end

				y = y - (font_height * (#lines - 1))		-- Readjust y
				if y <= -font_height then break end	-- If y is out of canvas bounds, stop writing
			end

			if self.isOpen then
				x = 5
				y = canvas_height - font_height - 5
				love.graphics.setFont(self.font)
				love.graphics.setColor(self.fontColor)

				love.graphics.line(0, y - (font_height / 2), canvas_width, y - (font_height / 2))	-- Input partition
				love.graphics.print(self._text, x, y)	-- User text
				local cursor_pos = self.font:getWidth(self._text:sub(1, self._cursor - 1))
				love.graphics.line(x + cursor_pos, y, x + cursor_pos, y + font_height)	-- Cursor
			end
		end)
		
		self._redraw = false
	end
	
	return self._canvas
end

Console.isOpen = false	-- Property to keep track of whether console is active


--[[
Private members
--]]							-- Drawable object for the console
Console._canvas			= love.graphics.newCanvas(Console.backgroundTexture:getDimensions())
Console._redraw			= true	-- Flag to keep track of whether the canvas needs to be redrawn
Console._inputs			= {}		-- Input history (most recent entry is at [1])
Console._inputIndex		= 0		-- _inputs index (for rolling through command history)
Console._outputs			= {}		-- Output history (most recent entry is at [1])
Console._outputIndex		= 1		-- _outputs index (for scrolling around the outputs)
Console._text			= ""		-- Input buffer
Console._cursor			= 1		-- Position in input buffer
Console._selection		= {}		-- User-selected text in Console
	--[[
	A selection is organized as {line,pos, line,pos}.
	The text selected is between each line's pos marker.
	Each pair represents _outputs[line]:sub(pos ...)
	Any `line` 0 represents the _text buffer instead.
	--]]

-- Containers to store callback functions that get overwritten while Console is open
Console._lovekeypressed		= love.keypressed
Console._lovekeyreleased		= love.keyreleased
Console._lovetextinput		= love.textinput
Console._lovemousepressed	= love.mousepressed
Console._lovemousereleased	= love.mousereleased
Console._lovewheelmoved		= love.wheelmoved


--[[
FText (FormattedText) prototype
--]]
Console.FText = {
	string	= "",
	font		= false,
	align		= 'left',
	em		= '',
	color		= false,
	highlight	= {0,0,0,0}
}


return Console
